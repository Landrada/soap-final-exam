package rw.ac.rcastockmngt.enums;

public enum ItemStatus {
    NEW, GOOD_SHAPE, OLD
}
