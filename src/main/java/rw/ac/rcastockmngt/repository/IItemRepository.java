package rw.ac.rcastockmngt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rw.ac.rcastockmngt.beans.Item;

public interface IItemRepository extends JpaRepository<Item,Integer> {

}
