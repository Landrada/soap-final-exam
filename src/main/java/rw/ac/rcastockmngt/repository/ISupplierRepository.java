package rw.ac.rcastockmngt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rw.ac.rcastockmngt.beans.Supplier;

public interface ISupplierRepository extends JpaRepository<Supplier, Integer> {

}
