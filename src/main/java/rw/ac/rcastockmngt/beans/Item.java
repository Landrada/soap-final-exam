package rw.ac.rcastockmngt.beans;

import rw.ac.rcastockmngt.enums.ItemStatus;

import javax.persistence.*;

@Entity
public class Item {
    public Item() {
    }

    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String itemCode;
    private int price;
    @Enumerated(EnumType.STRING)
    private ItemStatus status;
    private int supplier;

    public Item(int id, String name, String itemCode, int price,ItemStatus status, int supplier) {
        this.id = id;
        this.name = name;
        this.itemCode = itemCode;
        this.status = status;
        this.price = price;
        this.supplier = supplier;
    }
    public Item(int id, String name, String itemCode, int price, String status, int supplier) {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public ItemStatus getStatus() {
        return status;
    }

    public void setStatus(ItemStatus status) {
        this.status = status;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getSupplier() {
        return supplier;
    }

    public void setSupplier(int supplier) {
        this.supplier = supplier;
    }


}
