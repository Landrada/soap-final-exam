package rw.ac.rcastockmngt.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import rw.ac.rcastockmngnt.items.*;
import rw.ac.rcastockmngt.beans.Item;
import rw.ac.rcastockmngt.repository.IItemRepository;

import java.util.List;

@Endpoint
public class ItemDetailsEndPoint {
    @Autowired
    private IItemRepository itemRepository;

    @PayloadRoot(namespace = "http://rcastockmngnt.ac.rw/items", localPart = "GetItemDetailsRequest")
    @ResponsePayload
    public GetItemDetailsResponse findById(@RequestPayload GetItemDetailsRequest request) {

        Item item = itemRepository.findById(request.getId()).get();

        GetItemDetailsResponse itemDetailsResponse = mapItemDetail(item);
        return  itemDetailsResponse;
    }

    @PayloadRoot(namespace = "http://rcastockmngnt.ac.rw/items", localPart = "GetAllItemsDetailsRequest")
    @ResponsePayload
    public GetAllItemsDetailsResponse findAll(@RequestPayload GetAllItemsDetailsRequest request){
        GetAllItemsDetailsResponse allCourseDetailsResponse = new GetAllItemsDetailsResponse();

        List<Item> items = itemRepository.findAll();
        for (Item item: items){
            GetItemDetailsResponse itemDetailsResponse = mapItemDetail(item);
            allCourseDetailsResponse.getCourseDetails().add(itemDetailsResponse.getItemDetails());
        }
        return allCourseDetailsResponse;
    }


    @PayloadRoot(namespace = "http://rcastockmngnt.ac.rw/items", localPart = "CreateItemsDetailsRequest")
    @ResponsePayload
    public CreateItemsDetailsResponse save(@RequestPayload CreateItemsDetailsRequest request) {
        itemRepository.save(new Item(request.getItemDetails().getId(),
                request.getItemDetails().getName(),
                request.getItemDetails().getItemCode(),
                request.getItemDetails().getPrice(),
                request.getItemDetails().getStatus(),
                request.getItemDetails().getSupplier()
        ));

        CreateItemsDetailsResponse courseDetailsResponse = new CreateItemsDetailsResponse();
        courseDetailsResponse.setItemDetails(request.getItemDetails());
        courseDetailsResponse.setMessage("Created Successfully");
        return courseDetailsResponse;
    }

    @PayloadRoot(namespace = "http://rcastockmngnt.ac.rw/items", localPart = "DeleteItemsDetailsRequest")
    @ResponsePayload
    public DeleteItemsDetailsResponse save(@RequestPayload DeleteItemsDetailsRequest request) {

        System.out.println("ID: "+request.getId());
        itemRepository.deleteById(request.getId());

        DeleteItemsDetailsResponse itemsDetailsResponse = new DeleteItemsDetailsResponse();
        itemsDetailsResponse.setMessage("Deleted Successfully");
        return itemsDetailsResponse;
    }

    private GetItemDetailsResponse mapItemDetail(Item item){
        ItemDetails itemDetails = mapItem(item);

        GetItemDetailsResponse itemDetailsResponse = new GetItemDetailsResponse();

        itemDetailsResponse.setItemDetails(itemDetails);
        return itemDetailsResponse;
    }

    private UpdateItemsDetailsResponse mapItemDetails(Item item, String message) {
        ItemDetails itemDetails = mapItem(item);
        UpdateItemsDetailsResponse itemsDetailsResponse = new UpdateItemsDetailsResponse();

        itemsDetailsResponse.setItemDetails(itemDetails);
        itemsDetailsResponse.setMessage(message);
        return itemsDetailsResponse;
    }

    private ItemDetails mapItem(Item item){
        ItemDetails itemDetails = new ItemDetails();
        itemDetails.setId(item.getId());
        itemDetails.setName(item.getName());
        itemDetails.setPrice(item.getPrice());
        itemDetails.setStatus(item.getStatus().name());
        itemDetails.setSupplier(item.getSupplier());
        return itemDetails;
    }

}
