package rw.ac.rcastockmngt.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import rw.ac.rcastockmngnt.suppliers.*;
import rw.ac.rcastockmngt.beans.Supplier;
import rw.ac.rcastockmngt.repository.ISupplierRepository;

import java.util.List;

@Endpoint
public class SupplierDetailsEndPoint {
    @Autowired
    private ISupplierRepository supplierRepository;

    @PayloadRoot(namespace = "http://rcastockmngnt.ac.rw/suppliers", localPart = "GetSupplierDetailsRequest")
    @ResponsePayload
    public GetSupplierDetailsResponse findById(@RequestPayload GetSupplierDetailsRequest request) {

        Supplier supplier = supplierRepository.findById(request.getId()).get();

        GetSupplierDetailsResponse supplierDetailsResponse = mapSupplierDetail(supplier);
        return  supplierDetailsResponse;
    }

    @PayloadRoot(namespace = "http://rcastockmngnt.ac.rw/suppliers", localPart = "GetAllSupplierDetailsRequest")
    @ResponsePayload
    public GetAllSupplierDetailsResponse findAll(@RequestPayload GetAllSupplierDetailsRequest request){
        GetAllSupplierDetailsResponse allSupplierDetailsResponse = new GetAllSupplierDetailsResponse();

        List<Supplier> suppliers = supplierRepository.findAll();
        for (Supplier supplier: suppliers){
            GetSupplierDetailsResponse itemDetailsResponse = mapSupplierDetail(supplier);
            allSupplierDetailsResponse.getSupplierDetails().add(itemDetailsResponse.getSupplierDetails());
        }
        return allSupplierDetailsResponse;
    }

    private GetSupplierDetailsResponse mapSupplierDetail(Supplier supplier){
        SupplierDetails supplierDetails = mapSupplier(supplier);

        GetSupplierDetailsResponse supplierDetailsResponse = new GetSupplierDetailsResponse();

        supplierDetailsResponse.setSupplierDetails(supplierDetails);
        return supplierDetailsResponse;
    }

    private SupplierDetails mapSupplier(Supplier supplier){
        SupplierDetails supplierDetails = new SupplierDetails();
        supplierDetails.setId(supplier.getId());
        supplierDetails.setNames(supplier.getNames());
        supplierDetails.setEmail(supplier.getEmail());
        supplierDetails.setMobile(supplier.getMobile());
        return supplierDetails;
    }

}
