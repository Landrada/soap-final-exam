package rw.ac.rcastockmngt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RcaStockMngtApplication {

    public static void main(String[] args) {
        SpringApplication.run(RcaStockMngtApplication.class, args);
    }

}
